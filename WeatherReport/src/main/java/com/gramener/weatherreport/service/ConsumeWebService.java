package com.gramener.weatherreport.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.gramener.weatherreport.RestTemplate;

/*
 * this class used for WetherReport Service
 */

@Service
public class ConsumeWebService {
	boolean umbrella;
	JSONObject weatherReport;
	@Value("${openwhethermap.url}")
	private String currentWhetherUrl;
	@Value("${openwhethermap.weather}")
	private String weather;
	@Value("${openwhethermap.subscriptionkey}")
	private String subscriptionkey;
	@Value("${openwhethermap.id}")
	private String id;
	@Value("${openwhethermap.main}")
	private String main;
	@Value("${openwhethermap.temp}")
	private String temp;
	@Value("${openwhethermap.pressure}")
	private String pressure;
	@Value("${openwhethermap.message}")
	private String Message;
	@Value("${openwhethermap.keyTemp}")
	private String keyTemp;
	@Value("${openwhethermap.KeyPressure}")
	private String KeyPressure;
	@Value("${openwhethermap.keyUmbrella}")
	private String KeyUmbrella;
	@Value("${openwhethermap.errorMessage}")
	private String errorMessage;

	@Autowired
	RestTemplate restTemplate;

	/*
	 * this method used for WetherReport Service
	 */
	public JSONObject getData(String location) {
		try {

			String finalURL = currentWhetherUrl + location + ",uk&APPID=" + subscriptionkey;

			weatherReport = new JSONObject();
			String forObject = restTemplate.getForObject(finalURL, String.class);

			JSONObject responseJson = new JSONObject(forObject);

			JSONArray weatherJsonArray = responseJson.getJSONArray(weather);

			JSONObject idJSONObject = weatherJsonArray.getJSONObject(0);
			int weatherconditioncode = (int) idJSONObject.get(id);
			// 2XX:Thunderstorm; 3xx:Drizzle; 5xx: Rain
			if (weatherconditioncode >= 200 && weatherconditioncode <= 531) {
				umbrella = true;
			} else {
				umbrella = false;
			}
			JSONObject mainJsonArr = responseJson.getJSONObject(main);

			weatherReport.put(keyTemp, mainJsonArr.get(temp));
			weatherReport.put(KeyPressure, mainJsonArr.get(pressure));
			weatherReport.put(KeyUmbrella, umbrella);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			weatherReport.put(Message, errorMessage);
		}
		return weatherReport;
	}

}
