package com.gramener.weatherreport;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.gramener.weatherreport.service.ConsumeWebService;

/*
 * this class used for WeatherReport testing
 * 
 */
@ExtendWith(MockitoExtension.class)
class WeatherReportApplicationTests {

	public static final String TEMP = "Temp";
	public static final String UMBRELLA = "Umbrella";
	public static final String PRESSURE = "Pressure";
	public static final String TEMP_VALUE = "298.61";
	public static final Boolean UMBRELLA_VALUE = false;
	public static final String PRESSURE_VALUE = "1029";
	public static final String INPUT_VALUE = "London";
	
	@Mock
	ConsumeWebService consumeWebService;

	/*
	 * this test method used for WetherReport
	 */
	@Test
	public void weatherReportTestCase() {

		JSONObject excepted = null;
		try {
			excepted = new JSONObject();
			excepted.put(TEMP, TEMP_VALUE);
			excepted.put(UMBRELLA, UMBRELLA_VALUE);
			excepted.put(PRESSURE, PRESSURE_VALUE);
		} catch (JSONException e) {

			System.err.println(e.getMessage());
		}

		when(consumeWebService.getData(INPUT_VALUE)).thenReturn(excepted); // Specify what boolean value to return
		JSONObject actual = consumeWebService.getData(INPUT_VALUE);
		assertEquals(actual, excepted);
	}

}
